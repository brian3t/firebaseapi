#### Install
```
cp src/main/resources/application.properties.example src/main/resources/application.properties
```
Create src/main/resources/keys.json; which contains Firebase API password for each mobile app  

References:  
https://firebase.google.com/docs/auth/admin/manage-users  

#### Usage:
Please use this url to send notification to your MWCOG app:
http://[url]:8088/send/713687/ios?text=mylongtexthereasdf

format: http://[url]:8088/send/[commuter id]/[device platform]?text={the notification message}

Again, you can find out your commuter id and device platform by browsing http://socalappsolutions.com:8088/api/user
Let me know if you're able to send push notifications. Here's the maximum amount of notifications you can send in a minute:
https://firebase.google.com/docs/cloud-messaging/concept-options#device_throttling

To generate .jar or .war: ./mvnw clean package

To set up auto-deploy to wildfly; set correct username/password in 
`<deploy.wildfly.password>adminpw here</deploy.wildfly.password>`

