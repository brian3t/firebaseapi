-- run this AFTER spring-boot::run finished creating the tables

create or replace function update_timestamp()
returns trigger AS $$
begin
    NEW.updated_at = now();
    return NEW;
end ;$$
  language plpgsql VOLATILE
  COST 100;


alter table userr alter column created_at set default current_timestamp;

create trigger update_userr BEFORE update on userr FOR EACH ROW execute procedure update_timestamp();

alter table userr add constraint userr_unique_commid_dev_platform unique (commuter_id, dev_platform);

