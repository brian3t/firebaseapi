package com.mediabeef.firebaseapi;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class AdminController {
  private static final String template = "Hello, %s!";
  private final AtomicLong counter = new AtomicLong();

  @GetMapping("/admin")
  public Admin admin(
    @RequestParam(value = "name", defaultValue = "World")
    String name) throws FirebaseAuthException {
    UserRecord userRecord = FirebaseAuth.getInstance().getUserByEmail("ngxtri@gmail.com");
// See the UserRecord reference doc for the contents of userRecord.
    System.out.println("Successfully fetched user data: " + userRecord.getEmail());
    return new Admin(counter.incrementAndGet(), userRecord.getEmail());
  }
}
