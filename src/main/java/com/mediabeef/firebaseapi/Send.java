package com.mediabeef.firebaseapi;

public record Send(long id, String content) { }
