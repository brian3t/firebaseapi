package com.mediabeef.firebaseapi.controller;

import com.mediabeef.firebaseapi.model.User;
import com.mediabeef.firebaseapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class UserController {

  @Autowired
  UserRepository userRepository;

  @GetMapping("/user")
  public ResponseEntity<List<User>> getAllUsers(@RequestParam(required = false) String username,
                                                @RequestParam(required = false) Integer commuterId, @RequestParam(required = false) String devPlatform) {
    try {
      List<User> users = new ArrayList<User>();

      /*if (username == null)
        userRepository.findAll().forEach(users::add);
      else
        userRepository.findByUsernameContaining(username).forEach(users::add);*/

      if (commuterId == null) {
      } else {
        if (devPlatform == null) {
          userRepository.findByCommuterId(commuterId).forEach(users::add);
        } else {
          userRepository.findByCommuterIdAndDevPlatform(commuterId, devPlatform).forEach(users::add);
        }
        if (users.isEmpty()) {
          return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(users, HttpStatus.OK);
      }
      //no query param, find all
      users.addAll(userRepository.findAll());
      if (users.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(users, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/user/{id}")
  public ResponseEntity<User> getUserById(@PathVariable("id") long id) {
    Optional<User> userData = userRepository.findById(id);

    if (userData.isPresent()) {
      return new ResponseEntity<>(userData.get(), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping("/user")
  public ResponseEntity<Object> createUser(@RequestBody User user) {
    String pushToken = user.getPushToken();
    Integer commuterId = user.getCommuterId();
    String devPlatform = user.getDevPlatform();
    if (commuterId < 1) return new ResponseEntity<>("CommuterID cannot be empty", HttpStatus.BAD_REQUEST);
    if (pushToken.isEmpty()) return new ResponseEntity<>("Push token cannot be empty", HttpStatus.BAD_REQUEST);
    if (devPlatform.isEmpty()) return new ResponseEntity<>("Dev platform cannot be empty", HttpStatus.BAD_REQUEST);
    try {
      userRepository.deleteByCommuterIdAndDevPlatform(commuterId, devPlatform);
      User _user = userRepository
        .save(new User(user.getCommuterId(), user.getUsername(), user.getDevPlatform(), user.getPushToken()));
      return new ResponseEntity<>(_user, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PatchMapping("/user/{id}")
  public ResponseEntity<User> patchUser(@PathVariable("id") long id, @RequestBody User user) {
    Optional<User> userData = userRepository.findById(id);

    if (userData.isPresent()) {
      User _user = userData.get();
      Integer commuter_id = user.getCommuterId();
      if (commuter_id == null) { //skip
      } else {
        _user.setCommuterId(user.getCommuterId());
      }
      String username = user.getUsername();
      if (username == null) {
      } //skip
      else {
        _user.setUsername(username);
      }
      String dev_platform = user.getDevPlatform();
      if (dev_platform == null) {
      }//do thing
      else {
        _user.setDevPlatform(dev_platform);
      }
      String push_token = user.getPushToken();
      if (push_token == null) {
      }//skip
      else {
        _user.setPushToken(push_token);
      }
      return new ResponseEntity<>(userRepository.save(_user), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @PutMapping("/user/{id}")
  public ResponseEntity<Object> updateUser(@PathVariable("id") long id, @RequestBody User user) {
    Optional<User> userData = userRepository.findById(id);

    if (userData.isPresent()) {
      User _user = userData.get();
      Integer commuter_id = user.getCommuterId();
      if (commuter_id == null) {
        return new ResponseEntity<>("Missing commuterId", HttpStatus.BAD_REQUEST);
      }
      _user.setCommuterId(commuter_id);
      String username = user.getUsername();
      if (username == null) {
        return new ResponseEntity<>("Missing username", HttpStatus.BAD_REQUEST);
      }
      _user.setUsername(username);
      _user.setDevPlatform(user.getDevPlatform());
      _user.setPushToken(user.getPushToken());
      return new ResponseEntity<>(userRepository.save(_user), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/user/{id}")
  public ResponseEntity<HttpStatus> deleteUser(@PathVariable("id") long id) {
    try {
//			userRepository.deleteById(id);
      //todof Move to archive before delete
      return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/user/ios")
  public ResponseEntity<List<User>> findAllIos() {
    try {
      List<User> users = userRepository.findByDevPlatform("ios");

      if (users.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }
      return new ResponseEntity<>(users, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
