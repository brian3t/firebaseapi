package com.mediabeef.firebaseapi;

public record Admin(long id, String content) { }
