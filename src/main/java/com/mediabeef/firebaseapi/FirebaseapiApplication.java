package com.mediabeef.firebaseapi;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@SpringBootApplication
public class FirebaseapiApplication {
  private static final Properties properties;
  public static FirebaseApp simpleton_app;
  public static Map<String, String> KEYS = new HashMap<String, String>();
  public static final int DEFAULT_BUFFER_SIZE = 8192;

  static {
    properties = new Properties();

    try {
      ClassLoader classLoader = FirebaseapiApplication.class.getClassLoader();
      InputStream applicationPropertiesStream = classLoader.getResourceAsStream("application.properties");
      properties.load(applicationPropertiesStream);
    } catch (Exception e) {
      // process the exception
    }
  }

  public static void main(String[] args) throws IOException, FirebaseAuthException {
    String fcm_service_account_file = properties.getProperty("fcm_service_account_file");
	System.out.println(fcm_service_account_file);
   /* FileInputStream serviceAccount =
      new FileInputStream(fcm_service_account_file);*/
    ClassLoader classLoader = FirebaseapiApplication.class.getClassLoader();
    //InputStream serviceAccount = classLoader.getResourceAsStream(fcm_service_account_file);//
	InputStream serviceAccount = FirebaseapiApplication.class.getResourceAsStream(fcm_service_account_file);
	if (serviceAccount == null) System.out.println("Error. Service account file input stream is null!!");

//          String result = convertInputStreamToString(serviceAccount);

          //System.out.println(result);

    FirebaseOptions options = new FirebaseOptions.Builder()
      .setCredentials(GoogleCredentials.fromStream(serviceAccount))
      .build();

    ApplicationContext mContext = null;
    boolean hasBeenInitialized = false;
    List<FirebaseApp> firebaseApps = FirebaseApp.getApps();
    for (FirebaseApp app : firebaseApps) {
      if (app.getName().equals(FirebaseApp.DEFAULT_APP_NAME)) {
        hasBeenInitialized = true;
        simpleton_app = app;
      }
    }

    if (!hasBeenInitialized) {
      simpleton_app = FirebaseApp.initializeApp(options);
    }

    byte[] mapData;
//    System.out.println(System.getProperty("user.dir"));
    try (InputStream fin_keys = FirebaseapiApplication.class.getResourceAsStream("/keys.json")) {
      assert fin_keys != null;
      mapData = fin_keys.readAllBytes();
      Map<String, String> myMap = new HashMap<String, String>();
      ObjectMapper objectMapper = new ObjectMapper();
      myMap = objectMapper.readValue(mapData, new TypeReference<HashMap<String, String>>() {
      });
      System.out.println("Map using TypeReference: " + myMap);
      KEYS.put("mwcog", myMap.get("mwcog"));
    } catch (FileNotFoundException e) {
      System.out.print("ERR ");
      System.out.println(e.getMessage());
    }

    //verify token id
    /*// idToken comes from the client app (shown above)
    FirebaseToken decodedToken = FirebaseAuth.getInstance().verifyIdToken("eMoQguOXTrWl_tRUkrfn2w:APA91bGF4ykVL6XBDwm6-bGih-V16JszgKXHUdBBdXxgGwoWq001bQY6160WhK5SEte-OikAEVHonlmWYThZXf75G83lEBCFI5p645IimqsXD2DW7ukhqncn8qHiwDzIzIDzcxI1jGwL");
    String uid = decodedToken.getUid();
    System.out.print("decoded token: ");
    System.out.println(uid);*/
    SpringApplication.run(FirebaseapiApplication.class, args);
  }

  public static String get_token() throws FirebaseAuthException {
    String res = "";
    res = FirebaseAuth.getInstance().createCustomToken("ngxtri@gmail.com");
    /*FirebaseToken decodedToken = FirebaseAuth.getInstance().verifyIdToken(res);
    String uid = decodedToken.getUid();
    System.out.print("decoded token: ");
    System.out.println(uid);*/
    System.out.print("get_token response: ");
    System.out.println(res);
    return res;
  }

// Plain Java
    private static String convertInputStreamToString(InputStream is) throws IOException {

        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        int length;
        while ((length = is.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }

        // Java 1.1
        //return result.toString(StandardCharsets.UTF_8.name());

        return result.toString("UTF-8");

        // Java 10
        //return result.toString(StandardCharsets.UTF_8);

    }
}
