package com.mediabeef.firebaseapi;

public record Greeting(long id, String content) { }
