package com.mediabeef.firebaseapi.repository;

import com.mediabeef.firebaseapi.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {
//  List<User> findByPublished(boolean published);

  List<User> findByUsernameContaining(String username);
  List<User> findByDevPlatform(String dev_platform);

  List<User> findByCommuterId(Integer commuter_id);
  List<User> findByCommuterIdAndDevPlatform(Integer commuter_id, String dev_platform);

  long deleteByCommuterIdAndDevPlatform(
    @Param("commuter_id") Integer commuter_id,
    @Param("dev_platform") String dev_platform);

}

