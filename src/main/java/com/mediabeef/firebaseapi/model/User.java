package com.mediabeef.firebaseapi.model;

import javax.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
//import org.hibernate.query.criteria.JpaExpression;
import org.springframework.data.annotation.CreatedDate;

import java.sql.Timestamp;

@Entity
@Table(name = "userr")
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column(name = "commuter_id")
  private Integer commuterId;

  @Column(name = "username")
  private String username;

  @Column(name = "created_at", insertable = true, updatable = false)
  @CreationTimestamp //future, db should handle this. See folder ./sql
  private Timestamp createdAt;

  @Column(name = "updated_at", insertable = false, updatable = true)
//  @UpdateTimestamp //optional, db should handle this. See folder ./sql
  private Timestamp updatedAt;

  @Column(name = "dev_platform", length = 80)
  private String devPlatform;

  @Column(name = "push_token")
  private String pushToken;

  public User() {

  }

  public User(Integer commuter_id, String username, String devPlatform, String push_token) {
    this.commuterId = commuter_id;
    this.username = username;
    this.devPlatform = devPlatform;
    this.pushToken = push_token;
  }

  public long getId() {
    return id;
  }

  public Integer getCommuterId() {
    return commuterId;
  }

  public void setCommuterId(Integer commuter_id) {
    this.commuterId = commuter_id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getDevPlatform() {
    return devPlatform;
  }

  public void setDevPlatform(String dev_platform) {
    this.devPlatform = dev_platform;
  }

  public String getPushToken() {
    return pushToken;
  }

  public void setPushToken(String push_token) {
    this.pushToken = push_token;
  }

  @Override
  public String toString() {
    return "User [id=" + id + ", commuter_id=" + commuterId + ", username=" + username
      + ", dev_platform=" + devPlatform + ", push_token=" + pushToken + "]";
  }
}
