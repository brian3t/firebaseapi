package com.mediabeef.firebaseapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import com.mediabeef.firebaseapi.model.User;
import com.mediabeef.firebaseapi.repository.UserRepository;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class SendController {
  //  private static final boolean IS_DEBUG = false;
  private static final boolean IS_DEBUG = true;
  private static final String template = "Hello, %s!";
  private static final String BASE_URL = "https://fcm.googleapis.com/";
  //  private static final String BASE_URL = "http://api.demo/v1/test";//zsdf
  private static final String FCM_SEND_ENDPOINT = "fcm/send";
  //  private static final String FCM_SEND_ENDPOINT = "";//zsdf
  private final AtomicLong counter = new AtomicLong();
  @Autowired
  UserRepository userRepository;
  private String devPlatform;

  /* this is using new Cloud API
  public Send admin(
    @RequestParam(value = "text", defaultValue = "MWCOG notification")
    String text) throws FirebaseAuthException, IOException {
// See the UserRecord reference doc for the contents of userRecord.
    System.out.println("Sending noti..");
    URL url = new URL(BASE_URL + FCM_SEND_ENDPOINT);
    HttpURLConnection con = (HttpURLConnection) url.openConnection();
    con.setRequestProperty("Authorization", "Bearer " + getAccessToken());
    con.setRequestProperty("Content-Type", "application/json; UTF-8");
//    con.connect();
// For POST only - START
    con.setRequestMethod("POST");
		con.setDoOutput(true);
		OutputStream os = con.getOutputStream();
    String post_params = "";
		os.write(post_params.getBytes());
		os.flush();
		os.close();
		// For POST only - END

		int responseCode = con.getResponseCode();
		System.out.println("POST Response Code :: " + responseCode);

		if (responseCode == HttpURLConnection.HTTP_OK) { //success
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());
		} else {
			System.out.println("POST request did not work.");
		}
    return new Send(counter.incrementAndGet(), text);
  }*/

  /*
    Send noti using v1 url
   */
  @GetMapping("/send/{commuterId}/{devPlatform}")
  public ResponseEntity<Object> admin(@PathVariable("commuterId") int commuter_id, @PathVariable String devPlatform
    , String text, String mobileapp) throws FirebaseAuthException, IOException, URISyntaxException {
    String dev_platform = devPlatform;
//    System.out.println("Sending noti..");
    List<User> users = new ArrayList<User>(userRepository.findByCommuterIdAndDevPlatform(commuter_id, dev_platform));
    if (users.size() < 1) {
      return new ResponseEntity<>("Error: No such user", HttpStatus.NOT_ACCEPTABLE);
    }
    if (users.size() > 1) {
      return new ResponseEntity<>("Error, found more than one users: " + users.size() + ".id = " + users.get(0).getId()
        , HttpStatus.INTERNAL_SERVER_ERROR);
    }
    User user = users.get(0);
    String payload_str = "";
    String push_token = user.getPushToken();
    if (push_token.length() < 30){
      return new ResponseEntity<>("Error, bad push token: " + push_token, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    try {
      ObjectMapper obj_mapper = new ObjectMapper();
      NotiPayload ntpl = new NotiPayload();
      ntpl.data = new NpData();
      ntpl.data.title = "MWCOG";
      ntpl.data.message = text;
      ntpl.notification = new NpNoti();
      ntpl.notification.title = "MWCOG";
      ntpl.notification.body = text;
      ntpl.to = push_token;
      /*
      {
  "data":{
    "title":"MWCOG",
    "message":"Hello how are you?"
  },
  "notification": {"title":"MWCOG notif 0603c","message":"notif msg"},
  "to":"eMoQguOXTrWl_tRUkrfn2w:APA91bGF4ykVL6XBDwm6-bGih-V16JszgKXHUdBBdXxgGwoWq001bQY6160WhK5SEte-OikAEVHonlmWYThZXf75G83lEBCFI5p645IimqsXD2DW7ukhqncn8qHiwDzIzIDzcxI1jGwL"
}
       */

      payload_str = obj_mapper.writeValueAsString(ntpl);
      if (IS_DEBUG) System.out.println(payload_str);
      URL url = new URL(BASE_URL + FCM_SEND_ENDPOINT);
      CloseableHttpClient httpClient;
      try {
        httpClient = HttpClientBuilder.create().build();
      } catch (Exception e) {
        return new ResponseEntity<>("Error building Http Client", HttpStatus.INTERNAL_SERVER_ERROR);
      }
      HttpPost req = new HttpPost(url.toURI());
      StringEntity params = new StringEntity(payload_str);

      req.addHeader("Authorization", "key=" + get_key("mwcog"));
      req.addHeader("Content-Type", "application/json");
//    con.connect();
// For POST only - START
      req.setEntity(params);
      CloseableHttpResponse httpResponse = httpClient.execute(req);

      System.out.println("POST Response Status:: "
        + httpResponse.getStatusLine().getStatusCode());

      BufferedReader reader = new BufferedReader(new InputStreamReader(
        httpResponse.getEntity().getContent()));

      String inputLine;
      StringBuilder res = new StringBuilder();

      while ((inputLine = reader.readLine()) != null) {
        res.append(inputLine);
      }
      reader.close();

      // print result
      System.out.println(res.toString());

      // Creating Object of ObjectMapper define in Jackson
      // Api
      return new ResponseEntity<>(res.toString(), HttpStatus.OK);

    } catch (IOException e) {
      e.printStackTrace();
      return new ResponseEntity<>("Unexpected error sending noti" + e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  private String getAccessToken() throws FirebaseAuthException {
    return FirebaseapiApplication.get_token();
  }

  private String get_key(String app_name) {
    String res = "";
    return FirebaseapiApplication.KEYS.get(app_name);
  }
}
