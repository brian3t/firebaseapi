package com.mediabeef.firebaseapi;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class sample_code_fb {
    public static void sample() throws FileNotFoundException {
        try (FileInputStream serviceAccount = new FileInputStream("path/to/serviceAccountKey.json")) {
            System.out.println("service account key read successfully");
        } catch (FileNotFoundException e) {
            System.out.println("error");
        } catch (SecurityException | IOException e) {
            System.out.println("error sec");
        }

//        FirebaseOptions options = new FirebaseOptions.Builder()
//                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
//                .build();
//
//        FirebaseApp.initializeApp(options);

    }
}
